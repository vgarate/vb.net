﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim num1, num2 As Integer
        Dim result As Double
        num1 = 2
        num2 = 5
        'Tambien se puede usar num1 += 2 para sumar al valor existente o -= para restar
        result = num1 + num2
        MsgBox(result.ToString, MsgBoxStyle.OkOnly, "Suma")

        result = num1 - num2
        MsgBox(result.ToString, MsgBoxStyle.OkOnly, "Resta")

        result = num1 * num2
        MsgBox(result.ToString, MsgBoxStyle.OkOnly, "Multiplicación")

        result = num1 / num2
        MsgBox(result.ToString, MsgBoxStyle.OkOnly, "División")

        result = num1 Mod num2
        MsgBox(result.ToString, MsgBoxStyle.OkOnly, "Módulo")

        result = num1 ^ num2
        MsgBox(result.ToString, MsgBoxStyle.OkOnly, "Elevado")
    End Sub
End Class
