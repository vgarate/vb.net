﻿Public Class Form1
    Private Sub btnComparar_Click(sender As Object, e As EventArgs) Handles btnComparar.Click
        Dim dato As String = txtDatos.Text
        Dim cadena As String = "Palabra clave"

        If dato.Contains("Exit") Then
            MsgBox("Ha escrito Exit, el programa se cerrará", MsgBoxStyle.Information, "Salir")
            Me.Close()
        End If

        If dato.Contains(cadena) Then
            MsgBox("Palabra es igual", MsgBoxStyle.Information, "Aceptada")
        Else
            MsgBox("No cincide", MsgBoxStyle.Critical, "Error")
        End If

    End Sub

    Private Sub btnContiene_Click(sender As Object, e As EventArgs) Handles btnContiene.Click
        Dim compara As Integer
        ' StrComp entrega un valor -1, 0 y 1 segun corresponda <, = y > (cuenta el string)
        compara = StrComp(-500, -500)
        Select Case compara
            Case -1
                MsgBox("El valor 1 es menor", MsgBoxStyle.Information, "Primero es menor")
            Case 0
                MsgBox("Las 2 variables son iguales", MsgBoxStyle.Information, "Iguales")
            Case 1
                MsgBox("El valor 2 es menor", MsgBoxStyle.Information, "Segundo es menor")
        End Select

    End Sub
End Class
