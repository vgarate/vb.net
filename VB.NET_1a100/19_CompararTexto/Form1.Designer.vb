﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDatos = New System.Windows.Forms.TextBox()
        Me.btnComparar = New System.Windows.Forms.Button()
        Me.lbPalabraClave = New System.Windows.Forms.Label()
        Me.btnContiene = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(240, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(197, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Texto a comparar (Exit = salir)"
        '
        'txtDatos
        '
        Me.txtDatos.Location = New System.Drawing.Point(243, 77)
        Me.txtDatos.Name = "txtDatos"
        Me.txtDatos.Size = New System.Drawing.Size(194, 22)
        Me.txtDatos.TabIndex = 1
        '
        'btnComparar
        '
        Me.btnComparar.Location = New System.Drawing.Point(153, 188)
        Me.btnComparar.Name = "btnComparar"
        Me.btnComparar.Size = New System.Drawing.Size(138, 47)
        Me.btnComparar.TabIndex = 2
        Me.btnComparar.Text = "Comparar"
        Me.btnComparar.UseVisualStyleBackColor = True
        '
        'lbPalabraClave
        '
        Me.lbPalabraClave.AutoSize = True
        Me.lbPalabraClave.Location = New System.Drawing.Point(258, 118)
        Me.lbPalabraClave.Name = "lbPalabraClave"
        Me.lbPalabraClave.Size = New System.Drawing.Size(94, 17)
        Me.lbPalabraClave.TabIndex = 3
        Me.lbPalabraClave.Text = "Palabra clave"
        '
        'btnContiene
        '
        Me.btnContiene.Location = New System.Drawing.Point(419, 188)
        Me.btnContiene.Name = "btnContiene"
        Me.btnContiene.Size = New System.Drawing.Size(138, 47)
        Me.btnContiene.TabIndex = 4
        Me.btnContiene.Text = "Contiene"
        Me.btnContiene.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 453)
        Me.Controls.Add(Me.btnContiene)
        Me.Controls.Add(Me.lbPalabraClave)
        Me.Controls.Add(Me.btnComparar)
        Me.Controls.Add(Me.txtDatos)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtDatos As TextBox
    Friend WithEvents btnComparar As Button
    Friend WithEvents lbPalabraClave As Label
    Friend WithEvents btnContiene As Button
End Class
