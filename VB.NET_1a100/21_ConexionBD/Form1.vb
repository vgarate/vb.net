﻿
Public Class Form1
    Private cod As Integer
    Private nombre As String
    Private aPaterno As String
    Private aMaterno As String
    Private genero As String
    Private datos() As String

    Sub limpiar()
        txtCodEditar.Text = ""
        txtCodigoEleminar.Text = ""
        txtCodInsertar.Text = ""
        txtAMaterno.Text = ""
        txtAMaternoEditar.Text = ""
        txtAPaterno.Text = ""
        txtAPaternoEditar.Text = ""
        txtNombre.Text = ""
        txtNombreEditar.Text = ""
        cbGenero.Text = "Seleccionar"
        cbGeneroEditar.Text = "Seleccionar"
    End Sub

    Private Sub btnInsertar_Click(sender As Object, e As EventArgs) Handles btnInsertar.Click
        cod = txtCodInsertar.Text
        nombre = txtNombre.Text
        aPaterno = txtAPaterno.Text
        aMaterno = txtAMaterno.Text
        genero = cbGenero.Text

        insertar(cod, nombre, aPaterno, aMaterno, genero)
        limpiar()
        txtCodInsertar.Focus()

    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        datos = buscarb(txtCodEditar.Text)

        If Not IsNothing(datos) Then
            txtNombreEditar.Text = datos(0)
            txtAPaternoEditar.Text = datos(1)
            txtAMaternoEditar.Text = datos(2)
            cbGeneroEditar.Text = datos(3)
        Else
            limpiar()
            txtCodEditar.Focus()
        End If
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        cod = txtCodEditar.Text
        nombre = txtNombreEditar.Text
        aPaterno = txtAPaternoEditar.Text
        aMaterno = txtAMaternoEditar.Text
        genero = cbGeneroEditar.Text

        editar(cod, nombre, aPaterno, aMaterno, genero)
        limpiar()
        txtCodEditar.Focus()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        cod = txtCodigoEleminar.Text
        datos = buscarb(cod)

        Dim opt As Integer = MsgBox("Desea eliminar a " + datos(0) + "" + datos(1), MsgBoxStyle.YesNo, "ELIMINAR")
        If opt = DialogResult.Yes Then
            eliminar(cod)
            limpiar()
            txtCodigoEleminar.Focus()
        Else
            limpiar()
            txtCodigoEleminar.Focus()
        End If

    End Sub

    Private Sub btnMostrar_Click(sender As Object, e As EventArgs) Handles btnMostrar.Click
        llenarGrid()
    End Sub

    Private Sub llenarGrid()
        Dim ds As DataSet
        Dim dt As DataTable
        dt = buscarTodo()
        Me.DataGridView1.DataSource = dt
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        llenarComboEliminar()
    End Sub

    Public Sub llenarComboEliminar()

    End Sub
End Class
