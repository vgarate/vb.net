﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim dataTable As DataTable = New DataTable("tabla")
        dataTable.Columns.Add("Código")
        dataTable.Columns.Add("Descripción")

        Dim dataRow As DataRow

        dataRow = dataTable.NewRow()

        dataRow("Código") = 0
        dataRow("Descripción") = "Masculino"
        dataTable.Rows.Add(dataRow)

        dataRow = dataTable.NewRow()

        dataRow("Código") = 1
        dataRow("Descripción") = "Femenino"
        dataTable.Rows.Add(dataRow)

        cmbGenero.DataSource = dataTable
        cmbGenero.ValueMember = "Código"
        cmbGenero.DisplayMember = "Descripción"

    End Sub

    Private Sub btnSeleccion_Click(sender As Object, e As EventArgs) Handles btnSeleccion.Click
        MsgBox(cmbGenero.SelectedIndex.ToString)
    End Sub
End Class
