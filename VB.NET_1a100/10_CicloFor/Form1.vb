﻿Public Class Form1
    Private Sub btnFor_Click(sender As Object, e As EventArgs) Handles btnFor.Click
        Dim Frutas() As String = {"Manzana", "Pera", "Naranja"}
        Dim Matriz(2, 2) As Integer
        Dim i, j As Integer

        For i = 0 To Frutas.Length - 1
            MsgBox(Frutas(i), MsgBoxStyle.Information, "Recorriendo una array")
        Next

        For i = 0 To 1
            For j = 0 To 1
                Matriz(i, j) = (i + j + 2) * 2
                MsgBox(Matriz(i, j), MsgBoxStyle.Information, "Recorriendo una Matriz 2x2")
            Next
        Next
    End Sub
End Class
