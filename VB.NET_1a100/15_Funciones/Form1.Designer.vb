﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSaludar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnDespedir = New System.Windows.Forms.Button()
        Me.btnFuncion = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnSaludar
        '
        Me.btnSaludar.Location = New System.Drawing.Point(12, 46)
        Me.btnSaludar.Name = "btnSaludar"
        Me.btnSaludar.Size = New System.Drawing.Size(217, 56)
        Me.btnSaludar.TabIndex = 0
        Me.btnSaludar.Text = "Función Saludar"
        Me.btnSaludar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(458, 46)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(217, 56)
        Me.btnCerrar.TabIndex = 1
        Me.btnCerrar.Text = "Función Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnDespedir
        '
        Me.btnDespedir.Location = New System.Drawing.Point(235, 46)
        Me.btnDespedir.Name = "btnDespedir"
        Me.btnDespedir.Size = New System.Drawing.Size(217, 56)
        Me.btnDespedir.TabIndex = 2
        Me.btnDespedir.Text = "Función Despedirse"
        Me.btnDespedir.UseVisualStyleBackColor = True
        '
        'btnFuncion
        '
        Me.btnFuncion.Location = New System.Drawing.Point(12, 183)
        Me.btnFuncion.Name = "btnFuncion"
        Me.btnFuncion.Size = New System.Drawing.Size(217, 56)
        Me.btnFuncion.TabIndex = 3
        Me.btnFuncion.Text = "Procedimiento Saludar"
        Me.btnFuncion.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 453)
        Me.Controls.Add(Me.btnFuncion)
        Me.Controls.Add(Me.btnDespedir)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnSaludar)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnSaludar As Button
    Friend WithEvents btnCerrar As Button
    Friend WithEvents btnDespedir As Button
    Friend WithEvents btnFuncion As Button
End Class
