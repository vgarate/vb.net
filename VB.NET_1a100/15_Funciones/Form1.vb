﻿Public Class Form1
    ' Funciones sin parametros
    Sub saludo()
        MsgBox("Hola mundo desde la función saludar.", MsgBoxStyle.Information, "Saludar")
    End Sub

    Sub despedida()
        MsgBox("Despedida desde la función despedida.", MsgBoxStyle.Information, "Adiós")
    End Sub

    Sub cerrar()
        MsgBox("El programa se cerrará.")
        Me.Close()
    End Sub

    Private Sub btnSaludar_Click(sender As Object, e As EventArgs) Handles btnSaludar.Click
        saludo()
    End Sub

    Private Sub btnDespedir_Click(sender As Object, e As EventArgs) Handles btnDespedir.Click
        despedida()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        cerrar()
    End Sub

    'Procedimientos palabra reservada Function

    Function retornaSaludo()
        Dim saludo As String = "Hola desde un procedimiento para saludo"
        Return saludo
    End Function

    Private Sub btnFuncion_Click(sender As Object, e As EventArgs) Handles btnFuncion.Click
        Dim hola As String
        hola = retornaSaludo()
        MsgBox(hola, MsgBoxStyle.Information, "Procedimiento")
    End Sub
End Class
