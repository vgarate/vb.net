﻿Public Class Form1

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnResolver_Click(sender As Object, e As EventArgs) Handles btnResolver.Click
        Dim opc As String
        Dim n1, n2, resul As Double
        n1 = txtValor1.Text
        n2 = txtValor2.Text

        If rbSuma.Checked Then
            opc = "sumar"
        ElseIf rbResta.Checked Then
            opc = "resta"
        ElseIf rbMultiplicacion.Checked Then
            opc = "mult"
        ElseIf rbDivision.Checked Then
            If n2 > 0 Then
                opc = "divi"
            Else
                MsgBox("Ingrese un valor superior a 0 en el 'valor 2'", MsgBoxStyle.Critical, "Error al dividir")
                txtValor2.Focus()
            End If
        End If

        Select Case opc
            Case "sumar"
                resul = n1 + n2
            Case "resta"
                resul = n1 - n2
            Case "mult"
                resul = n1 * n2
            Case "divi"
                resul = n1 / n2

        End Select
        lbSolucion.Text = resul.ToString
    End Sub
End Class
