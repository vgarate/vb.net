﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnResolver = New System.Windows.Forms.Button()
        Me.lbResultado = New System.Windows.Forms.Label()
        Me.lbSolucion = New System.Windows.Forms.Label()
        Me.lbNumero2 = New System.Windows.Forms.Label()
        Me.lbNumero1 = New System.Windows.Forms.Label()
        Me.txtNumero2 = New System.Windows.Forms.TextBox()
        Me.txtNumero1 = New System.Windows.Forms.TextBox()
        Me.rbDividir = New System.Windows.Forms.RadioButton()
        Me.rbMultiplicar = New System.Windows.Forms.RadioButton()
        Me.rbRestar = New System.Windows.Forms.RadioButton()
        Me.rbSumar = New System.Windows.Forms.RadioButton()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnResolver)
        Me.GroupBox1.Controls.Add(Me.lbResultado)
        Me.GroupBox1.Controls.Add(Me.lbSolucion)
        Me.GroupBox1.Controls.Add(Me.lbNumero2)
        Me.GroupBox1.Controls.Add(Me.lbNumero1)
        Me.GroupBox1.Controls.Add(Me.txtNumero2)
        Me.GroupBox1.Controls.Add(Me.txtNumero1)
        Me.GroupBox1.Controls.Add(Me.rbDividir)
        Me.GroupBox1.Controls.Add(Me.rbMultiplicar)
        Me.GroupBox1.Controls.Add(Me.rbRestar)
        Me.GroupBox1.Controls.Add(Me.rbSumar)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(423, 200)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'btnResolver
        '
        Me.btnResolver.Location = New System.Drawing.Point(228, 99)
        Me.btnResolver.Name = "btnResolver"
        Me.btnResolver.Size = New System.Drawing.Size(189, 36)
        Me.btnResolver.TabIndex = 10
        Me.btnResolver.Text = "Resolver"
        Me.btnResolver.UseVisualStyleBackColor = True
        '
        'lbResultado
        '
        Me.lbResultado.AutoSize = True
        Me.lbResultado.Location = New System.Drawing.Point(156, 156)
        Me.lbResultado.Name = "lbResultado"
        Me.lbResultado.Size = New System.Drawing.Size(0, 17)
        Me.lbResultado.TabIndex = 9
        '
        'lbSolucion
        '
        Me.lbSolucion.AutoSize = True
        Me.lbSolucion.Location = New System.Drawing.Point(86, 156)
        Me.lbSolucion.Name = "lbSolucion"
        Me.lbSolucion.Size = New System.Drawing.Size(70, 17)
        Me.lbSolucion.TabIndex = 8
        Me.lbSolucion.Text = "Solución :"
        '
        'lbNumero2
        '
        Me.lbNumero2.AutoSize = True
        Me.lbNumero2.Location = New System.Drawing.Point(206, 65)
        Me.lbNumero2.Name = "lbNumero2"
        Me.lbNumero2.Size = New System.Drawing.Size(70, 17)
        Me.lbNumero2.TabIndex = 7
        Me.lbNumero2.Text = "Numero 2"
        '
        'lbNumero1
        '
        Me.lbNumero1.AutoSize = True
        Me.lbNumero1.Location = New System.Drawing.Point(206, 32)
        Me.lbNumero1.Name = "lbNumero1"
        Me.lbNumero1.Size = New System.Drawing.Size(70, 17)
        Me.lbNumero1.TabIndex = 6
        Me.lbNumero1.Text = "Numero 1"
        '
        'txtNumero2
        '
        Me.txtNumero2.Location = New System.Drawing.Point(282, 60)
        Me.txtNumero2.Name = "txtNumero2"
        Me.txtNumero2.Size = New System.Drawing.Size(135, 22)
        Me.txtNumero2.TabIndex = 5
        Me.txtNumero2.Text = "0"
        '
        'txtNumero1
        '
        Me.txtNumero1.Location = New System.Drawing.Point(282, 32)
        Me.txtNumero1.Name = "txtNumero1"
        Me.txtNumero1.Size = New System.Drawing.Size(135, 22)
        Me.txtNumero1.TabIndex = 4
        Me.txtNumero1.Text = "0"
        '
        'rbDividir
        '
        Me.rbDividir.AutoSize = True
        Me.rbDividir.Location = New System.Drawing.Point(25, 114)
        Me.rbDividir.Name = "rbDividir"
        Me.rbDividir.Size = New System.Drawing.Size(68, 21)
        Me.rbDividir.TabIndex = 3
        Me.rbDividir.TabStop = True
        Me.rbDividir.Text = "Dividir"
        Me.rbDividir.UseVisualStyleBackColor = True
        '
        'rbMultiplicar
        '
        Me.rbMultiplicar.AutoSize = True
        Me.rbMultiplicar.Location = New System.Drawing.Point(25, 87)
        Me.rbMultiplicar.Name = "rbMultiplicar"
        Me.rbMultiplicar.Size = New System.Drawing.Size(92, 21)
        Me.rbMultiplicar.TabIndex = 2
        Me.rbMultiplicar.TabStop = True
        Me.rbMultiplicar.Text = "Multiplicar"
        Me.rbMultiplicar.UseVisualStyleBackColor = True
        '
        'rbRestar
        '
        Me.rbRestar.AutoSize = True
        Me.rbRestar.Location = New System.Drawing.Point(25, 60)
        Me.rbRestar.Name = "rbRestar"
        Me.rbRestar.Size = New System.Drawing.Size(71, 21)
        Me.rbRestar.TabIndex = 1
        Me.rbRestar.TabStop = True
        Me.rbRestar.Text = "Restar"
        Me.rbRestar.UseVisualStyleBackColor = True
        '
        'rbSumar
        '
        Me.rbSumar.AutoSize = True
        Me.rbSumar.Location = New System.Drawing.Point(25, 33)
        Me.rbSumar.Name = "rbSumar"
        Me.rbSumar.Size = New System.Drawing.Size(65, 21)
        Me.rbSumar.TabIndex = 0
        Me.rbSumar.TabStop = True
        Me.rbSumar.Text = "Suma"
        Me.rbSumar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(154, 370)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(171, 56)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(447, 502)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtNumero1 As TextBox
    Friend WithEvents rbDividir As RadioButton
    Friend WithEvents rbMultiplicar As RadioButton
    Friend WithEvents rbRestar As RadioButton
    Friend WithEvents rbSumar As RadioButton
    Friend WithEvents btnResolver As Button
    Friend WithEvents lbResultado As Label
    Friend WithEvents lbSolucion As Label
    Friend WithEvents lbNumero2 As Label
    Friend WithEvents lbNumero1 As Label
    Friend WithEvents txtNumero2 As TextBox
    Friend WithEvents btnSalir As Button
End Class
