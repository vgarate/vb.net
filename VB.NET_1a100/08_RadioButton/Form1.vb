﻿Public Class Form1
    Private Sub btnResolver_Click(sender As Object, e As EventArgs) Handles btnResolver.Click
        Dim resul, n1, n2 As Double

        n1 = txtNumero1.Text
        n2 = txtNumero2.Text

        If rbSumar.Checked Then
            resul = n1 + n2
            lbResultado.Text = resul.ToString
        ElseIf rbRestar.Checked Then
            resul = n1 - n2
            lbResultado.Text = resul.ToString
        ElseIf rbMultiplicar.Checked Then
            resul = n1 * n2
            lbResultado.Text = resul.ToString
        ElseIf rbDividir.Checked Then
            If n2 > 0 Then
                resul = n1 / n2
                lbResultado.Text = resul.ToString
            Else
                MsgBox("Debe ingresar un número mayor a 0 en el segundo número para la división", MsgBoxStyle.Critical, "Error al dividir")
                txtNumero2.Focus()
            End If
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNumero1.Focus()

    End Sub
End Class
