﻿Public Class Form1
    Dim numero1, numero2 As Integer
    Dim resultado As Double


    Private Sub btnSumar_Click(sender As Object, e As EventArgs) Handles btnSumar.Click
        numero1 = txtNumero1.Text
        numero2 = txtNumero2.Text
        resultado = numero1 + numero2
        lbResultado.Text = resultado.ToString

    End Sub

    Private Sub btnRestar_Click(sender As Object, e As EventArgs) Handles btnRestar.Click
        numero1 = txtNumero1.Text
        numero2 = txtNumero2.Text
        resultado = numero1 - numero2
        lbResultado.Text = resultado.ToString
    End Sub

    Private Sub btnMultiplicar_Click(sender As Object, e As EventArgs) Handles btnMultiplicar.Click
        numero1 = txtNumero1.Text
        numero2 = txtNumero2.Text
        resultado = numero1 * numero2
        lbResultado.Text = resultado.ToString
    End Sub

    Private Sub btnDividir_Click(sender As Object, e As EventArgs) Handles btnDividir.Click
        numero1 = txtNumero1.Text
        numero2 = txtNumero2.Text
        If numero2 < 1 Then
            MsgBox("Debe ingresar un numero superior a 0 para la división", MsgBoxStyle.Critical, "Error")
            txtNumero2.Focus()
            lbResultado.Text = ""
        Else
            resultado = numero1 / numero2
            lbResultado.Text = resultado.ToString
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub txtNumero1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumero1.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtNumero2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumero2.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        txtNumero1.Text = 0
        txtNumero2.Text = 0
        lbResultado.Text = ""
        txtNumero1.Focus()
    End Sub

End Class
