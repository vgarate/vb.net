﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSumar = New System.Windows.Forms.Button()
        Me.btnRestar = New System.Windows.Forms.Button()
        Me.btnDividir = New System.Windows.Forms.Button()
        Me.txtNumero1 = New System.Windows.Forms.TextBox()
        Me.txtNumero2 = New System.Windows.Forms.TextBox()
        Me.lbResultado = New System.Windows.Forms.Label()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.btnMultiplicar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lbNumero1 = New System.Windows.Forms.Label()
        Me.lbNumero2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnSumar
        '
        Me.btnSumar.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnSumar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSumar.ForeColor = System.Drawing.Color.Black
        Me.btnSumar.Location = New System.Drawing.Point(12, 159)
        Me.btnSumar.Name = "btnSumar"
        Me.btnSumar.Size = New System.Drawing.Size(110, 41)
        Me.btnSumar.TabIndex = 0
        Me.btnSumar.Text = "Sumar"
        Me.btnSumar.UseVisualStyleBackColor = False
        '
        'btnRestar
        '
        Me.btnRestar.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnRestar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRestar.ForeColor = System.Drawing.Color.Black
        Me.btnRestar.Location = New System.Drawing.Point(128, 159)
        Me.btnRestar.Name = "btnRestar"
        Me.btnRestar.Size = New System.Drawing.Size(110, 41)
        Me.btnRestar.TabIndex = 1
        Me.btnRestar.Text = "Restar"
        Me.btnRestar.UseVisualStyleBackColor = False
        '
        'btnDividir
        '
        Me.btnDividir.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnDividir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDividir.ForeColor = System.Drawing.Color.Black
        Me.btnDividir.Location = New System.Drawing.Point(360, 159)
        Me.btnDividir.Name = "btnDividir"
        Me.btnDividir.Size = New System.Drawing.Size(110, 41)
        Me.btnDividir.TabIndex = 3
        Me.btnDividir.Text = "Dividir"
        Me.btnDividir.UseVisualStyleBackColor = False
        '
        'txtNumero1
        '
        Me.txtNumero1.Location = New System.Drawing.Point(128, 48)
        Me.txtNumero1.Name = "txtNumero1"
        Me.txtNumero1.Size = New System.Drawing.Size(226, 22)
        Me.txtNumero1.TabIndex = 4
        Me.txtNumero1.Text = "0"
        '
        'txtNumero2
        '
        Me.txtNumero2.Location = New System.Drawing.Point(128, 104)
        Me.txtNumero2.Name = "txtNumero2"
        Me.txtNumero2.Size = New System.Drawing.Size(226, 22)
        Me.txtNumero2.TabIndex = 6
        Me.txtNumero2.Text = "0"
        '
        'lbResultado
        '
        Me.lbResultado.AutoSize = True
        Me.lbResultado.Location = New System.Drawing.Point(238, 228)
        Me.lbResultado.Name = "lbResultado"
        Me.lbResultado.Size = New System.Drawing.Size(0, 17)
        Me.lbResultado.TabIndex = 7
        Me.lbResultado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnLimpiar
        '
        Me.btnLimpiar.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnLimpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiar.ForeColor = System.Drawing.Color.Black
        Me.btnLimpiar.Location = New System.Drawing.Point(360, 48)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(110, 78)
        Me.btnLimpiar.TabIndex = 8
        Me.btnLimpiar.Text = "Limpiar"
        Me.btnLimpiar.UseVisualStyleBackColor = False
        '
        'btnMultiplicar
        '
        Me.btnMultiplicar.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnMultiplicar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMultiplicar.ForeColor = System.Drawing.Color.Black
        Me.btnMultiplicar.Location = New System.Drawing.Point(241, 159)
        Me.btnMultiplicar.Name = "btnMultiplicar"
        Me.btnMultiplicar.Size = New System.Drawing.Size(110, 41)
        Me.btnMultiplicar.TabIndex = 9
        Me.btnMultiplicar.Text = "Multiplicar"
        Me.btnMultiplicar.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(360, 228)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(110, 78)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'lbNumero1
        '
        Me.lbNumero1.AutoSize = True
        Me.lbNumero1.Location = New System.Drawing.Point(52, 48)
        Me.lbNumero1.Name = "lbNumero1"
        Me.lbNumero1.Size = New System.Drawing.Size(70, 17)
        Me.lbNumero1.TabIndex = 11
        Me.lbNumero1.Text = "Numero 1"
        '
        'lbNumero2
        '
        Me.lbNumero2.AutoSize = True
        Me.lbNumero2.Location = New System.Drawing.Point(52, 104)
        Me.lbNumero2.Name = "lbNumero2"
        Me.lbNumero2.Size = New System.Drawing.Size(70, 17)
        Me.lbNumero2.TabIndex = 12
        Me.lbNumero2.Text = "Numero 2"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(482, 453)
        Me.Controls.Add(Me.lbNumero2)
        Me.Controls.Add(Me.lbNumero1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnMultiplicar)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.lbResultado)
        Me.Controls.Add(Me.txtNumero2)
        Me.Controls.Add(Me.txtNumero1)
        Me.Controls.Add(Me.btnDividir)
        Me.Controls.Add(Me.btnRestar)
        Me.Controls.Add(Me.btnSumar)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnSumar As Button
    Friend WithEvents btnRestar As Button
    Friend WithEvents btnDividir As Button
    Friend WithEvents txtNumero1 As TextBox
    Friend WithEvents txtNumero2 As TextBox
    Friend WithEvents lbResultado As Label
    Friend WithEvents btnLimpiar As Button
    Friend WithEvents btnMultiplicar As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents lbNumero1 As Label
    Friend WithEvents lbNumero2 As Label
End Class
