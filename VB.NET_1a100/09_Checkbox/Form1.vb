﻿Public Class Form1
    Private Sub btnPreferencias_Click(sender As Object, e As EventArgs) Handles btnPreferencias.Click
        Dim nombre As String

        If Me.cbManzana.Checked Then
            nombre += " Manzana"
        End If
        If Me.cbPera.Checked Then
            nombre += " Pera"
        End If
        If Me.cbDamasco.Checked Then
            nombre += " Damasco"
        End If
        If Me.cbDurazno.Checked Then
            nombre += " Durazno"
        End If
        If Me.cbNaranja.Checked Then
            nombre += " Naranja"
        End If
        If nombre = "" Then
            lbEleccionUsuario.Text = "Ninguna selección"
        Else
            lbEleccionUsuario.Text = nombre
        End If

    End Sub
End Class
