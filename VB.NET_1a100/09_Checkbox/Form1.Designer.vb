﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbTituloSeleccion = New System.Windows.Forms.Label()
        Me.cbManzana = New System.Windows.Forms.CheckBox()
        Me.cbPera = New System.Windows.Forms.CheckBox()
        Me.cbNaranja = New System.Windows.Forms.CheckBox()
        Me.cbDurazno = New System.Windows.Forms.CheckBox()
        Me.cbDamasco = New System.Windows.Forms.CheckBox()
        Me.btnPreferencias = New System.Windows.Forms.Button()
        Me.lbTituloEleccion = New System.Windows.Forms.Label()
        Me.lbEleccionUsuario = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbTituloSeleccion
        '
        Me.lbTituloSeleccion.AutoSize = True
        Me.lbTituloSeleccion.Location = New System.Drawing.Point(48, 26)
        Me.lbTituloSeleccion.Name = "lbTituloSeleccion"
        Me.lbTituloSeleccion.Size = New System.Drawing.Size(129, 17)
        Me.lbTituloSeleccion.TabIndex = 0
        Me.lbTituloSeleccion.Text = "Selección de frutas"
        '
        'cbManzana
        '
        Me.cbManzana.AutoSize = True
        Me.cbManzana.Location = New System.Drawing.Point(46, 57)
        Me.cbManzana.Name = "cbManzana"
        Me.cbManzana.Size = New System.Drawing.Size(88, 21)
        Me.cbManzana.TabIndex = 1
        Me.cbManzana.Text = "Manzana"
        Me.cbManzana.UseVisualStyleBackColor = True
        '
        'cbPera
        '
        Me.cbPera.AutoSize = True
        Me.cbPera.Location = New System.Drawing.Point(46, 84)
        Me.cbPera.Name = "cbPera"
        Me.cbPera.Size = New System.Drawing.Size(60, 21)
        Me.cbPera.TabIndex = 2
        Me.cbPera.Text = "Pera"
        Me.cbPera.UseVisualStyleBackColor = True
        '
        'cbNaranja
        '
        Me.cbNaranja.AutoSize = True
        Me.cbNaranja.Location = New System.Drawing.Point(46, 111)
        Me.cbNaranja.Name = "cbNaranja"
        Me.cbNaranja.Size = New System.Drawing.Size(80, 21)
        Me.cbNaranja.TabIndex = 3
        Me.cbNaranja.Text = "Naranja"
        Me.cbNaranja.UseVisualStyleBackColor = True
        '
        'cbDurazno
        '
        Me.cbDurazno.AutoSize = True
        Me.cbDurazno.Location = New System.Drawing.Point(46, 138)
        Me.cbDurazno.Name = "cbDurazno"
        Me.cbDurazno.Size = New System.Drawing.Size(84, 21)
        Me.cbDurazno.TabIndex = 4
        Me.cbDurazno.Text = "Durazno"
        Me.cbDurazno.UseVisualStyleBackColor = True
        '
        'cbDamasco
        '
        Me.cbDamasco.AutoSize = True
        Me.cbDamasco.Location = New System.Drawing.Point(46, 165)
        Me.cbDamasco.Name = "cbDamasco"
        Me.cbDamasco.Size = New System.Drawing.Size(89, 21)
        Me.cbDamasco.TabIndex = 5
        Me.cbDamasco.Text = "Damasco"
        Me.cbDamasco.UseVisualStyleBackColor = True
        '
        'btnPreferencias
        '
        Me.btnPreferencias.Location = New System.Drawing.Point(12, 224)
        Me.btnPreferencias.Name = "btnPreferencias"
        Me.btnPreferencias.Size = New System.Drawing.Size(175, 43)
        Me.btnPreferencias.TabIndex = 6
        Me.btnPreferencias.Text = "Enviar preferencia"
        Me.btnPreferencias.UseVisualStyleBackColor = True
        '
        'lbTituloEleccion
        '
        Me.lbTituloEleccion.AutoSize = True
        Me.lbTituloEleccion.Location = New System.Drawing.Point(417, 42)
        Me.lbTituloEleccion.Name = "lbTituloEleccion"
        Me.lbTituloEleccion.Size = New System.Drawing.Size(154, 17)
        Me.lbTituloEleccion.TabIndex = 7
        Me.lbTituloEleccion.Text = "La elección del usuario"
        '
        'lbEleccionUsuario
        '
        Me.lbEleccionUsuario.AutoSize = True
        Me.lbEleccionUsuario.Location = New System.Drawing.Point(375, 90)
        Me.lbEleccionUsuario.Name = "lbEleccionUsuario"
        Me.lbEleccionUsuario.Size = New System.Drawing.Size(0, 17)
        Me.lbEleccionUsuario.TabIndex = 8
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.lbEleccionUsuario)
        Me.Controls.Add(Me.lbTituloEleccion)
        Me.Controls.Add(Me.btnPreferencias)
        Me.Controls.Add(Me.cbDamasco)
        Me.Controls.Add(Me.cbDurazno)
        Me.Controls.Add(Me.cbNaranja)
        Me.Controls.Add(Me.cbPera)
        Me.Controls.Add(Me.cbManzana)
        Me.Controls.Add(Me.lbTituloSeleccion)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbTituloSeleccion As Label
    Friend WithEvents cbManzana As CheckBox
    Friend WithEvents cbPera As CheckBox
    Friend WithEvents cbNaranja As CheckBox
    Friend WithEvents cbDurazno As CheckBox
    Friend WithEvents cbDamasco As CheckBox
    Friend WithEvents btnPreferencias As Button
    Friend WithEvents lbTituloEleccion As Label
    Friend WithEvents lbEleccionUsuario As Label
End Class
