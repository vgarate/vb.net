﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim entero As Integer = 2
        MsgBox(entero, MsgBoxStyle.OkOnly, "Variable entero asignada")
        entero = 0
        MsgBox(entero, MsgBoxStyle.OkOnly, "Variable entero reasignada")

        Dim doble As Double = 123.123
        MsgBox(doble, MsgBoxStyle.OkOnly, "Variable tipo double")

        Dim texto As String = "Hola Mundo"
        MsgBox(texto, MsgBoxStyle.OkOnly, "Variable string")
        Dim texto2 As String = ", Vicente"
        MsgBox(texto + texto2, MsgBoxStyle.OkOnly, "Texto anidado")

        Dim fecha As Date = "09/08/84"
        MsgBox(fecha, MsgBoxStyle.OkOnly, "Tipo fecha")
    End Sub
End Class
