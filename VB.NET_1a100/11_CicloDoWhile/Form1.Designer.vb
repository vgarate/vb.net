﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCicloDo = New System.Windows.Forms.Button()
        Me.btnCicloDoWhile = New System.Windows.Forms.Button()
        Me.btnWhile = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnCicloDo
        '
        Me.btnCicloDo.Location = New System.Drawing.Point(216, 68)
        Me.btnCicloDo.Name = "btnCicloDo"
        Me.btnCicloDo.Size = New System.Drawing.Size(119, 51)
        Me.btnCicloDo.TabIndex = 0
        Me.btnCicloDo.Text = "Ciclo DO"
        Me.btnCicloDo.UseVisualStyleBackColor = True
        '
        'btnCicloDoWhile
        '
        Me.btnCicloDoWhile.Location = New System.Drawing.Point(216, 125)
        Me.btnCicloDoWhile.Name = "btnCicloDoWhile"
        Me.btnCicloDoWhile.Size = New System.Drawing.Size(119, 51)
        Me.btnCicloDoWhile.TabIndex = 1
        Me.btnCicloDoWhile.Text = "Ciclo DO While"
        Me.btnCicloDoWhile.UseVisualStyleBackColor = True
        '
        'btnWhile
        '
        Me.btnWhile.Location = New System.Drawing.Point(216, 182)
        Me.btnWhile.Name = "btnWhile"
        Me.btnWhile.Size = New System.Drawing.Size(119, 51)
        Me.btnWhile.TabIndex = 2
        Me.btnWhile.Text = "Ciclo While"
        Me.btnWhile.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(216, 239)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(119, 51)
        Me.btnSalir.TabIndex = 3
        Me.btnSalir.Text = "SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(558, 481)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnWhile)
        Me.Controls.Add(Me.btnCicloDoWhile)
        Me.Controls.Add(Me.btnCicloDo)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnCicloDo As Button
    Friend WithEvents btnCicloDoWhile As Button
    Friend WithEvents btnWhile As Button
    Friend WithEvents btnSalir As Button
End Class
