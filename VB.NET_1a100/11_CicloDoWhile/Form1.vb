﻿Public Class Form1
    Dim i As Integer

    Private Sub btnCicloDo_Click(sender As Object, e As EventArgs) Handles btnCicloDo.Click
        i = 0
        Do
            MsgBox("Inicio ciclo DO: " + i.ToString)
            i += 1
        Loop While i < 3
    End Sub

    Private Sub btnCicloDoWhile_Click(sender As Object, e As EventArgs) Handles btnCicloDoWhile.Click
        i = 0
        Do While i < 3
            MsgBox("Ciclo DO WHILE: " + i.ToString)
            i += 1
        Loop
    End Sub

    Private Sub btnWhile_Click(sender As Object, e As EventArgs) Handles btnWhile.Click
        i = 0
        While i < 3
            MsgBox("Ciclo WHILE: " + i.ToString)
            i += 1
        End While
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class
