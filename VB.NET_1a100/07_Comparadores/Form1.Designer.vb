﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtExpresion1 = New System.Windows.Forms.TextBox()
        Me.txtExpresion2 = New System.Windows.Forms.TextBox()
        Me.btnComparar = New System.Windows.Forms.Button()
        Me.lbExpresion1 = New System.Windows.Forms.Label()
        Me.lbExpresion2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbIgual = New System.Windows.Forms.Label()
        Me.lbMayor = New System.Windows.Forms.Label()
        Me.lbMenor = New System.Windows.Forms.Label()
        Me.lbMayorIgual = New System.Windows.Forms.Label()
        Me.lbMenorIgual = New System.Windows.Forms.Label()
        Me.lbDistinto = New System.Windows.Forms.Label()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtExpresion1
        '
        Me.txtExpresion1.Location = New System.Drawing.Point(68, 99)
        Me.txtExpresion1.Name = "txtExpresion1"
        Me.txtExpresion1.Size = New System.Drawing.Size(203, 22)
        Me.txtExpresion1.TabIndex = 0
        '
        'txtExpresion2
        '
        Me.txtExpresion2.Location = New System.Drawing.Point(280, 99)
        Me.txtExpresion2.Name = "txtExpresion2"
        Me.txtExpresion2.Size = New System.Drawing.Size(203, 22)
        Me.txtExpresion2.TabIndex = 1
        '
        'btnComparar
        '
        Me.btnComparar.Location = New System.Drawing.Point(68, 143)
        Me.btnComparar.Name = "btnComparar"
        Me.btnComparar.Size = New System.Drawing.Size(203, 42)
        Me.btnComparar.TabIndex = 2
        Me.btnComparar.Text = "Comparar"
        Me.btnComparar.UseVisualStyleBackColor = True
        '
        'lbExpresion1
        '
        Me.lbExpresion1.AutoSize = True
        Me.lbExpresion1.Location = New System.Drawing.Point(114, 75)
        Me.lbExpresion1.Name = "lbExpresion1"
        Me.lbExpresion1.Size = New System.Drawing.Size(82, 17)
        Me.lbExpresion1.TabIndex = 3
        Me.lbExpresion1.Text = "Expresión 1"
        '
        'lbExpresion2
        '
        Me.lbExpresion2.AutoSize = True
        Me.lbExpresion2.Location = New System.Drawing.Point(338, 75)
        Me.lbExpresion2.Name = "lbExpresion2"
        Me.lbExpresion2.Size = New System.Drawing.Size(82, 17)
        Me.lbExpresion2.TabIndex = 4
        Me.lbExpresion2.Text = "Expresión 2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(197, 205)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 17)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "¿Es igual?"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(160, 236)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "¿Es mayor que?"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(159, 264)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 17)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "¿Es menor que?"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(114, 295)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(157, 17)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "¿Es mayor o igual que?"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(113, 326)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(158, 17)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "¿Es menor o igual que?"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(182, 357)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "¿Es distinto?"
        '
        'lbIgual
        '
        Me.lbIgual.AutoSize = True
        Me.lbIgual.Location = New System.Drawing.Point(277, 205)
        Me.lbIgual.Name = "lbIgual"
        Me.lbIgual.Size = New System.Drawing.Size(0, 17)
        Me.lbIgual.TabIndex = 11
        '
        'lbMayor
        '
        Me.lbMayor.AutoSize = True
        Me.lbMayor.Location = New System.Drawing.Point(277, 236)
        Me.lbMayor.Name = "lbMayor"
        Me.lbMayor.Size = New System.Drawing.Size(0, 17)
        Me.lbMayor.TabIndex = 12
        '
        'lbMenor
        '
        Me.lbMenor.AutoSize = True
        Me.lbMenor.Location = New System.Drawing.Point(277, 264)
        Me.lbMenor.Name = "lbMenor"
        Me.lbMenor.Size = New System.Drawing.Size(0, 17)
        Me.lbMenor.TabIndex = 13
        '
        'lbMayorIgual
        '
        Me.lbMayorIgual.AutoSize = True
        Me.lbMayorIgual.Location = New System.Drawing.Point(277, 295)
        Me.lbMayorIgual.Name = "lbMayorIgual"
        Me.lbMayorIgual.Size = New System.Drawing.Size(0, 17)
        Me.lbMayorIgual.TabIndex = 14
        '
        'lbMenorIgual
        '
        Me.lbMenorIgual.AutoSize = True
        Me.lbMenorIgual.Location = New System.Drawing.Point(277, 326)
        Me.lbMenorIgual.Name = "lbMenorIgual"
        Me.lbMenorIgual.Size = New System.Drawing.Size(0, 17)
        Me.lbMenorIgual.TabIndex = 15
        '
        'lbDistinto
        '
        Me.lbDistinto.AutoSize = True
        Me.lbDistinto.Location = New System.Drawing.Point(277, 357)
        Me.lbDistinto.Name = "lbDistinto"
        Me.lbDistinto.Size = New System.Drawing.Size(0, 17)
        Me.lbDistinto.TabIndex = 16
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Location = New System.Drawing.Point(280, 143)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(203, 42)
        Me.btnLimpiar.TabIndex = 17
        Me.btnLimpiar.Text = "Limpiar"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(117, 393)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(303, 42)
        Me.btnSalir.TabIndex = 18
        Me.btnSalir.Text = "SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(580, 464)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.lbDistinto)
        Me.Controls.Add(Me.lbMenorIgual)
        Me.Controls.Add(Me.lbMayorIgual)
        Me.Controls.Add(Me.lbMenor)
        Me.Controls.Add(Me.lbMayor)
        Me.Controls.Add(Me.lbIgual)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbExpresion2)
        Me.Controls.Add(Me.lbExpresion1)
        Me.Controls.Add(Me.btnComparar)
        Me.Controls.Add(Me.txtExpresion2)
        Me.Controls.Add(Me.txtExpresion1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtExpresion1 As TextBox
    Friend WithEvents txtExpresion2 As TextBox
    Friend WithEvents btnComparar As Button
    Friend WithEvents lbExpresion1 As Label
    Friend WithEvents lbExpresion2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lbIgual As Label
    Friend WithEvents lbMayor As Label
    Friend WithEvents lbMenor As Label
    Friend WithEvents lbMayorIgual As Label
    Friend WithEvents lbMenorIgual As Label
    Friend WithEvents lbDistinto As Label
    Friend WithEvents btnLimpiar As Button
    Friend WithEvents btnSalir As Button
End Class
