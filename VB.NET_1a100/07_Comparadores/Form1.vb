﻿Public Class Form1
    Dim uno, dos

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        txtExpresion1.Text = ""
        txtExpresion2.Text = ""
        lbIgual.Text = ""
        lbMayorIgual.Text = ""
        lbMenorIgual.Text = ""
        lbMayor.Text = ""
        lbMenor.Text = ""
        lbDistinto.Text = ""
        txtExpresion1.Focus()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub btnComparar_Click(sender As Object, e As EventArgs) Handles btnComparar.Click
        uno = txtExpresion1.Text
        dos = txtExpresion2.Text

        If uno = dos Then
            lbIgual.Text = "Si"
            lbMayorIgual.Text = "Si"
            lbMenorIgual.Text = "Si"
            lbMayor.Text = "No"
            lbMenor.Text = "No"
            lbDistinto.Text = "No"
        Else
            If uno > dos Then
                lbIgual.Text = "No"
                lbMayorIgual.Text = "Si"
                lbMenorIgual.Text = "No"
                lbMayor.Text = "Si"
                lbMenor.Text = "No"
                lbDistinto.Text = "Si"
            Else
                lbIgual.Text = "No"
                lbMayorIgual.Text = "No"
                lbMenorIgual.Text = "Si"
                lbMayor.Text = "No"
                lbMenor.Text = "Si"
                lbDistinto.Text = "Si"
            End If
        End If

    End Sub
End Class
