﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim arreglo(3) As String
        arreglo(0) = "Hola"
        arreglo(1) = "Mundo,"
        arreglo(2) = "Vicente"
        MsgBox(arreglo(0) + " " + arreglo(1) + " producido por " + arreglo(2), MsgBoxStyle.OkOnly, "Arreglo tipo string")

        Dim arreglo2() As Integer
        arreglo2 = New Integer() {1, 2, 3}
        MsgBox(arreglo2(0).ToString + ", " + arreglo2(1).ToString, MsgBoxStyle.OkOnly, "Arrays de numeros mostrados con .ToString")

        Dim matriz(,) As Integer = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
        MsgBox(matriz(2, 0).ToString, MsgBoxStyle.OkOnly, "Matriz de 3x3")
    End Sub
End Class